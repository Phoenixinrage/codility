package lesseon1;

public class BinaryGap {
    public static void main(String[] args) {
        int test = 72;
        System.out.println(Integer.toBinaryString(test));
        System.out.println(binaryGap(test));
    }

    public static int binaryGap(int num) {
        String binary = Integer.toBinaryString(num);
        int result = 0;
        int gap = 0;
        boolean start = false;
        for (char c : binary.toCharArray()) {
            if ('1'  == c && gap == 0) {
                start = true;
            } else if ('1' == c && gap > 0) {
                result = Math.max(result, gap);
                gap = 0;
            } else if ('0' == c && start) {
                gap++;
            }
        }
        return result;
    }
}
