package lesson3timecomplexity;

public class TapeEquilibrium {
    public static void main(String[] args) {
        System.out.println(minDifference(new int[] {10, 0, 10, 10, 0, 10}));
    }

    public static int minDifference(int[] nums) {
        int sum = 0;
        for(int i : nums) {
            sum += i;
        }
        int diff = sum;
        int left = 0;
        int right = sum;
        for (int i = 0; i < nums.length; i++) {
            left += nums[i];
            right -= nums[i];
            diff = Math.min(diff, Math.abs(left - right));
        }
        return diff;
    }
}
