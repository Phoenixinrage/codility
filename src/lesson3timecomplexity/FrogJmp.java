package lesson3timecomplexity;

public class FrogJmp {
    public static void main(String[] args) {
        System.out.println(minJumps(10, 41, 30));
    }

    public static int minJumps(int start, int end, int jump) {
        int distance = end - start;
        int res = distance / jump;
        return distance % jump > 0 ? res + 1 : res;
    }
}
