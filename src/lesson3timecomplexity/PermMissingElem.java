package lesson3timecomplexity;

public class PermMissingElem {
    public static void main(String[] args) {
        System.out.println(findMissing(new int[] {1,2,3,5,4,7}));
    }

    public static int findMissing(int[] ints) {
        int totalSum = ((ints.length + 1) * (ints.length + 2)) / 2;
        int sum = 0;
        for(int i : ints) {
            sum +=i;
        }
        return totalSum - sum;
    }
}
