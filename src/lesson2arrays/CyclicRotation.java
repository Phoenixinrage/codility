package lesson2arrays;

import java.util.Arrays;

public class CyclicRotation {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(cycleArray(new int[] {3, 8, 9, 7, 6}, 3)));
    }

    public static int[] cycleArray(int[] arr, int k) {
        if (arr.length < k) {
            k = k % arr.length;
        }
        int[] res = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            int sourceIndex = (i + arr.length - k) % arr.length;
            res[i] = arr[sourceIndex];
        }
        return res;
    }
}
