package lesson2arrays;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class OddOccurrencesInArray {
    public static void main(String[] args) {
        System.out.println(extractOdd(new int[] {1,1,2,2,3,3,3}));
    }

    public static int extractOdd(int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int i : arr) {
            map.merge(i, 1, Integer::sum);
        }
        Optional<Integer> result = map.entrySet().stream()
                .filter(e -> e.getValue() % 2 > 0)
                .map(Map.Entry::getKey)
                .findAny();
        return result.orElse(-1);
    }
}
